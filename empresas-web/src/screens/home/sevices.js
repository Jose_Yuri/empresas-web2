import api from "../../API/baseApi";

export default class HomeServices {
  static async getEmpresas(token, client, uid) {
    try {
      const response = await api.get("/enterprises", {
        headers: { "access-token": token, client: client, uid: uid },
      });
      return response.data;
      //return 401;
    } catch (Error) {
      console.log(Error.response);
      return Error.response.status;
    }
  }
}
