import React, { useState, useContext } from 'react';
import { useHistory } from 'react-router-dom'
import "./home-style.css"
import axios from 'axios'
import Nabar from '../../Components/NavBar/navbar'
import SearchBar from '../../Components/SearchNavBar/searchbar'
import Card from '../../Components/Card/card'
import List from '../../Components/List/list'
import HomeService from './sevices'
import LoadingModal from '../../Components/LoadingModal/loadingModal'
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
const Home = () => {
    const [control, setControl] = useState(false)
    const routerControl = useHistory()
    const [contextValue, setValue] = useState([])
    const [filter,setFilter] = useState("")
    const [openModal,setModal] = useState(false);
    const [showTimeoutModal,setShowTimeOutModal] = useState(false)

    const handleShow = async () => {
        setControl(true)
        setModal(true)
        const token = localStorage.getItem("access-token")
        const uid = localStorage.getItem("uid")
        const client = localStorage.getItem("client")
        const response = await HomeService.getEmpresas(token,client,uid)
        if(response === 401){
            setShowTimeOutModal(true)
            setModal(false)
            setTimeout(()=>{setShowTimeOutModal(false)
                localStorage.clear()
                routerControl.push('/')
            },2000)
            
        }
        else{
            setModal(false)
            setValue(response.enterprises)
        }
       
       
    }
    return (
            <div className="_Home">
                <nav style={{position:'fixed'}}>
                    {!control ? <Nabar Control={handleShow} /> : <SearchBar filter={(value)=>setFilter(value)} setControl = {() =>setControl()}  />}
                </nav>
                <>
                    {!control  ? <div id='inicio'>  <h2 className="Clique-na-busca-para">  Clique na busca para iniciar. </h2> </div>  : <LoadingModal open={openModal}/>  }
                    {(control && openModal === false) &&  <div id='lista'>  <List list={contextValue} filter={filter} /> </div>}
                </>
                <Dialog open={showTimeoutModal} >
                    <DialogTitle>  Erro </DialogTitle>
                    <div  style={{width:'80%',margin:'auto'}}>
                        <h4> Timeout de Acesso, você será redirecionado a tela de login </h4>
                    </div>
                   
                </Dialog>
            </div>

    )
}

export default Home