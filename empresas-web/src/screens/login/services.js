import api from "../../API/baseApi";

export default class LoginService {
  static async Login(email, senha) {
    try {
      const response = await api.post("users/auth/sign_in", {
        email: email,
        password: senha,
      });
      localStorage.setItem("access-token", response.headers["access-token"]);
      localStorage.setItem("client", response.headers["client"]);
      localStorage.setItem("uid", response.headers["uid"]);
      console.log(response, "resposta");
      return response.data;
    } catch (Error) {
      console.log(Error.response.data);
      return Error.response.data;
    }
  }
}
