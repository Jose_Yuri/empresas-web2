import React, { useState } from 'react';
import { useHistory } from 'react-router-dom'
import "./login-style.css"

import {Input,InputAdornment,IconButton,Button } from '@material-ui/core';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import {Modal} from "react-bootstrap"
import Logo from "../../Assets/logo-home.png"
import Logo2x from '../../Assets/logo-home@2x.png'
import Logo3x from '../../Assets/logo-home@3x.png'
import MailIcon from '../../Assets/ic-email.png'
import MailIcon2x from '../../Assets/ic-email@2x.png'
import MailIcon3x from '../../Assets/ic-email@3x.png'
import LockIcon from '../../Assets/ic-cadeado.png'
import LockIcon2x from '../../Assets/ic-cadeado@2x.png'
import LockIcon3x from '../../Assets/ic-cadeado@3x.png'

import LoadingModal from '../../Components/LoadingModal/loadingModal'

import axios from 'axios'

import LoginService from './services'

const Login = () => {
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [loginError,setLoginError] = useState(true)
    const [errorMessage,setErrorMessage] = useState('')
    const [openModal,setModal] = useState(false);
    const [showPassword,setShowPassword] = useState(false) 
  
    const routeHook = useHistory();


    const handleLogin = async (e) => {
       
        e.preventDefault()
        setModal(true)
       
        const response =  await LoginService.Login(email,password)
        if(response.success){
            setModal(false)
            routeHook.push("/home")
        }
        else{
            setModal(false)
            setErrorMessage(response.errors)
            setLoginError(response.success)
        }
    }
       
    const handleEmailInput = (e) => {
        const {target} = e
        setEmail(target.value);
        setLoginError(true)
    }

    const handlePaswordInput = (e) =>{
        const{target} = e
        setPassword(target.value)
        setLoginError(true)
    }

    const MostrarSenha = () =>{
        !showPassword ?  setShowPassword(true) : setShowPassword(false)
       
    }
    
    return (
        <>
        
        <div className="_Login">
            <div className="row">
                <img className="logo_home" src={Logo} srcSet={`${Logo2x} 2x, ${Logo3x} 3x`} ></img>
            </div>
            <div className="row" >
                <h1 className="BEM-VINDO-AO-EMPRESA" > Bem Vindo Ao Empresas </h1>
            </div>
            <div className="row">
                <h2 className="Lorem-ipsum-dolor-si"> Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan.</h2>
            </div>

            <form onSubmit={handleLogin}>
                <div className='teste'>
                <Input style={{width:'27.1rem'}} placeholder='Email' onChange={(event)=>handleEmailInput(event)}  startAdornment={
                    <InputAdornment position='start'>
                        <img  src={MailIcon} srcSet={`${MailIcon2x} 2x, ${MailIcon3x} 3x`} ></img>
                    </InputAdornment>
                } endAdornment={
                    !loginError  &&  <InputAdornment position='end'>
                    <span className="Oval erroIcon"> ! </span> 
              </InputAdornment>
                }
                />
                </div>
                <div>
                <Input type={showPassword ? 'text' : 'password'} defaultValue={''} placeholder='Senha'  onChange={(event)=>handlePaswordInput(event)} startAdornment={
                    <InputAdornment position='start'>
                        <img src={LockIcon} srcSet={`${LockIcon2x} 2x, ${LockIcon3x} 3x`} ></img>
                    </InputAdornment>
                } endAdornment={
                    loginError ?
                    <InputAdornment position='end'>
                        <IconButton onClick={MostrarSenha} aria-label="toggle password visibility">
                            {showPassword ? <Visibility/>  : <VisibilityOff/>  }
                        </IconButton>
                    </InputAdornment> :
                      <InputAdornment position='end'>
                        <span className="Oval erroIcon"> ! </span> 
                  </InputAdornment>
                } />
                </div>
                {
                    !loginError && <div className="row" >
                    <p className="Credenciais-informad"> Credenciais informadas são inválidas, tente novamente. </p>
                  </div>
                }
                <div className="loginButton">
                    <Button type="submit" fullWidth variant="contained" color="primary" className='button-login'> Entrar </Button>
                </div>
            </form>
            <LoadingModal open={openModal} />
        </div>
            
        </>
    )
}

export default Login