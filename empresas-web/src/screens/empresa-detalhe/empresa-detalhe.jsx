import React from 'react';
import Empresa from '../../Assets/img-e-1-lista.png'
import './empresa-detalhe.css'
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import { useHistory } from 'react-router-dom'


const Detalhe = (props) => {
    const { location } = props
    const routerControl = useHistory()
    console.log(location.state)
    return (
        <div className="_Home" >
            <nav className='NavDetalhe'>
                <div onClick={routerControl.goBack}  className='ArrowBackDetalhe' style={{marginLeft:'2.625rem'}}>
                    <ArrowBackIcon id="arrowBackDetalhe" style={{fontSize:'2.188rem'}} color="primary" style={{alignSelf:'initial'}}/>
                </div>
                <div style={{marginLeft:'2.5rem'}}>
                   <p className='Empresa'>  {location.state.empresa.enterprise_name} </p>
                </div>
            </nav>
            <div>
            <div id="cardDetalhe" >
               <Card className='CardDetalhe'>
                   <div>
                       <CardMedia className='CardDetalheImagem' component="img" image={Empresa} />
                   </div>
                   <div className='textBox' >
                       <CardContent>
                           <div >
                             <p className='textContentDetalhe'> {location.state.empresa.description} </p>
                           </div>
                       </CardContent>
                   </div>
               </Card>
            </div>
        </div>
        </div>
    )
}

export default Detalhe