import axios from "axios";

const baseAPI = axios.create({
  baseURL: "https://empresas.ioasys.com.br/api/v1/",
});

export default baseAPI;
