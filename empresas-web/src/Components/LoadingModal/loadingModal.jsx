import React from 'react'
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import CircularProgress from '@material-ui/core/CircularProgress';

import './loadingModal.css'

const LoadingModal = (props) => {
    const {onClose,selectedValue,open} = props;

    const handleClose = () =>{
        onClose(selectedValue);
    }

    

    return(
        <Dialog id='loading' onClose={handleClose}  open={open}   PaperProps={{
            style: {
              backgroundColor: 'transparent',
              boxShadow: 'none',
            },
          }}>
        
         <CircularProgress size={100} color="secondary" />
        </Dialog>    
    )
}

export default LoadingModal