import React from 'react';
import CardList from '../Card/card'

import {Grid} from '@material-ui/core'
import "./list-style.css"
import LoadingModal from '../LoadingModal/loadingModal'
const List = (props) => {
    
    const {list,filter} = props
   
    return (
        <Grid  style={{width:'70rem' ,marginTop:'9.063rem'}} container  >
             {(filter == '') ? list.map(elment => <Grid item  style={{marginBottom:'24px',marginTop:'24px',marginLeft:'6rem'}} xs={12}>  <CardList empresa = {elment}/>  </Grid>):
                 list.filter(elment2 => elment2.enterprise_name.toLowerCase().includes(filter.toLowerCase())).map(result =>  <Grid item style={{marginBottom:'24px',marginLeft:'6rem'}} xs={12}>  <CardList empresa = {result}/>  </Grid>)
             }
        </Grid>
    )
}

export default List