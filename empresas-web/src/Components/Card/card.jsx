import React, { useState } from 'react'
import { useHistory } from 'react-router-dom'
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Empresa from '../../Assets/img-e-1-lista.png'
import "./card-style.css"


const CardList = (props) => {
    const routeHook = useHistory();
    const {empresa} = props
    return (
        <>
            
            <Card onClick={()=>routeHook.push("/detalhe",{empresa:empresa})} style={{display:'flex'}}>
                <div>
                  <CardMedia id="cardImage" component="img" image={Empresa}/>
                </div>
                <div  >
                    <CardContent className="cardResume" >
                        <p  className="Empresa1"> {empresa.enterprise_name} </p>
                         <p className="Negcio" > {empresa.enterprise_type.enterprise_type_name} </p>
                         <p className="Brasil">  {empresa.country} </p>
                    </CardContent>
                </div>
                
            </Card>
        </>
      
    )
}


export default CardList