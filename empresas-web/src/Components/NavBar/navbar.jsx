import React, { useState } from 'react';
import Logo from "../../Assets/logo-nav.png"
import Logo2x from '../../Assets/logo-nav@2x.png'
import Logo3x from '../../Assets/logo-nav@3x.png'
import Search from "../../Assets/ic-search-copy.png"
import Search2x from '../../Assets/ic-search-copy@2x.png'
import Search3x from '../../Assets/ic-search-copy@3x.png'
import "./navbarStyle.css"
const navBar = props => {
    const {Control} = props
    return (
        <>
            <div style={{marginTop:'2%'}}>
                <img  className="brand-logo center" src={Logo} srcSet={`${Logo2x} 2x, ${Logo3x} 3x`} ></img>
            </div>
            <div style={{position:'absolute',right:'-0.557rem',top:'-0.3rem'}}>
            <img onClick={()=>Control()} className=" ic_search-copy" src={Search} srcSet={`${Search2x} 2x, ${Search3x} 3x`} ></img>
            </div>
        </>
    )
}

export default navBar