import React, { useState } from 'react';

import {useHistory} from 'react-router-dom'
import {Input,InputAdornment,IconButton,Button } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import Search from "../../Assets/ic-search-copy.png"
import Search2x from '../../Assets/ic-search-copy@2x.png'
import Search3x from '../../Assets/ic-search-copy@3x.png'
import './searchbar-style.css'
const Searchbar = (props,params) =>{
   
    const {filter,setControl} = props
    
    
    return(
        <>
              <form>
                <div  className="brand-logo centerSearchBar search inputAlign" > 
                   <Input autoComplete='off' id="searchInput"  onChange={(e)=>filter(e.target.value)} placeholder='Pesquisa' startAdornment={
                       <InputAdornment style={{marginTop:'-0.9rem'}} position='start'>
                           <img  src={Search} srcSet={`${Search2x} 2x, ${Search3x} 3x`} ></img>
                       </InputAdornment>
                   }  endAdornment={
                       <InputAdornment position='end'>
                            <IconButton onClick={() => setControl(true)} aria-label="toggle password visibility">
                            <CloseIcon id='closeSearch' color='primary'/>
                        </IconButton>
                       </InputAdornment>
                   } />
                </div>
            </form>
            
           
      </>
    )
}

export default Searchbar